=================================================
Integrated Modelling System Infrastructure (IMSI)
=================================================

.. image:: https://readthedocs.org/projects/imsi/badge/?version=latest
        :target: https://imsi.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status


The Integrated Modelling System Infrastructure (IMSI) is a comprehensive python based package used to download, configure, 
build and run the suite of models in the CCCma Integrated Modelling System.


* Documentation: https://imsi.readthedocs.io.

License
-------
* `Open Government License - Canada version 2.0 <https://open.canada.ca/en/open-government-licence-canada>`_

Installation
------------

You can install imsi as below. It is recommendad to install it in a python or conda virtual environment.
    
.. code-block::  

    #python3 -m venv /path/to/new/virtual/environment # or
    # conda create -n imsi-test python=3.10


    git clone git@gitlab.com:cccma/imsi.git
    cd imsi
    pip install .      # for usage
    # pip install -e . # for development
    
Basic usage
-----------

.. code-block:: 

    imsi -h # for help
    #
    # Setup a CanESM5.0 p2 CMIP6 piControl run
    imsi setup --repo=git@gitlab.com:cccma/canesm.git --ver=v5.1_cp4c --exp=cmip6-piControl --model=canesm51_p1 --runid=imsi-test
    imsi build  # compile executables 
    ./save_restart_files.sh
    imsi submit # submit the run
    imsi qstat  # check a pbs queue





    