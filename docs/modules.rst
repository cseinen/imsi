.. _imsiAPI:

The imsi API
================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This section describes the :program:`imsi` Application Programming Interface 
(API). 


Command line interface (cli.py)
-------------------------------

.. automodule:: imsi.cli
   :members:
   :undoc-members:
   :show-inheritance:

Main working module (imsi_main.py)
----------------------------------

.. automodule:: imsi.imsi_main
   :members:
   :undoc-members:
   :show-inheritance:

Utility functions (utils.py)
-----------------------------

.. automodule:: imsi.utils
   :members:
   :undoc-members:
   :show-inheritance:

Namelist tools (nml_tools.py)
-----------------------------

.. automodule:: imsi.nml_tools
   :members:
   :undoc-members:
   :show-inheritance:

Git tools (git_tools.py)
------------------------

.. automodule:: imsi.git_tools
   :members:
   :undoc-members:
   :show-inheritance:

Scheduler tools (scheduler_tools.py)
------------------------------------

.. automodule:: imsi.scheduler_tools
   :members:
   :undoc-members:
   :show-inheritance:

Sequencer caps (sequencers.py)
------------------------------

.. automodule:: imsi.sequencers
   :members:
   :undoc-members:
   :show-inheritance:

Imsi shell sequencer (iss.py)
-----------------------------

.. automodule:: imsi.iss
   :members:
   :undoc-members:
   :show-inheritance:

