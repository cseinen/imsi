=====
Usage
=====

Model simulation workflow
-------------------------

The basic steps to conduct a model simulation are:

1. setup the run (`imsi-setup`)
2. Build the run exectuables (`imsi build`)
3. Save required restart files 
4. Submit the run

Configuring user defaults
-------------------------

The file

.. code-block:: bash

    ~/.imsi/imsi-user-config.json

can be used to set user preferences and options. These are always respected over any
other choices [Work in Progress].


Setting up a run
----------------

Using `imsi setup` to create a run directory, obtain the model source code,
and extract all required model configuration files. The setup will result
in an imsi configured directory, from which futher interaction with the run
is conducted. 

The output of `imsi setup -h`:

.. program-output:: imsi setup -h

The imsi setup creates some subdirectories for the run, including `bin` (for executables),
and `config`. The `config` directory contains various run configuration files, including
cpp directives and namelists, that have been extracted and modified according to the choice
of model and experiment.

The setup also creates a log file (`.imsi-setup.log`), an imsi state file (`.imsi_simulation_state.pkl`),
and a readable/writable json file containing the full details of the model configuration options 
(`imsi_configuration_<runid>.json`). A `save_restarts_files.sh` is extracted to allow saving of files to
initiate the run. This must be executed before `imsi submit`.

Example
+++++++

.. code-block:: bash

    imsi setup --repo=git@gitlab.com:swartn/canesm.git --ver=jack_0.1 --exp=cmip6-piControl --model=canesm50_p2 --runid=imsi-test


Quering available configurations
--------------------------------

Use `imsi list` to query the supported machines, compilers, models and experiments
in a given imsi repository. If you are already in an `imsi` configured directory,
it will be based on the local model repository. If you are not in an imsi-configured
directory, you can point towards the relevant model directory to scan for supported
options (using `--repo`).

The output of `imsi list -h`:

.. program-output:: imsi list -h


Example
+++++++

.. code-block:: bash

    imsi list -e --repo=/home/$USER/canesm

        Supported experiments: cmip6-1pctCO2 cmip6-abrupt-4xCO2 cmip6-piControl cmip6-amip cmip6-historical 

Modifying basic run parameters
------------------------------

To modify an run paramters, there are basically 4 choices, in order of preference:

1. Use `imsi set` to apply a specific option or selection (see set usage below)

2. Modify the upstream `.json` files in the `imsi-config` directory (of the model source code in the run directory) 
   and change settings as required. Once complete, run `imsi update`, which will reparse the source and update the
   configuration. Modifying in such a way is highly desirable, because any changes can easily be commited, reused and 
   shared.

3. Modify the `imsi_configuration_<runid>.json` file, and run `imsi config`. This will ingest the json file and apply
   all of its settings into the current configuration. This is less desirable as it is easy to make inconsistent changes,
   and any changes are not easily resuable.

4. Edit files in the `config` directory directly. The files in the `config` directory are the "single source of truth", in other
   words, they are the files that will be used by the model simulation. While editing them directly might be convenient for fast 
   tests, the changes are not easily captured or resused, and this method is prone to internal consistency errors.


The output of `imsi set -h`:

.. program-output:: imsi set -h


Example
+++++++

Choose the configuration using 2+4 nodes (atm-ocean):

.. code-block:: bash

    imsi set -o pe_config=2+4

Building run executables
------------------------

The output of `imsi build -h`:

.. program-output:: imsi setup -h


Adding new options for future reuse
------------------------------------

Options allow for selection from a range or choices, similar to what would be provided by an if statement 
or switch in scripting. In the `imsi-config` directory of the source code, modify the 
`models/model_options.json`, and add options following the examples provided. Any imsi setting can 
be modified via this mechanism. 

Adding new models or experiments
--------------------------------

Add a new model or experiment file. Start from an experiment that is as close to your target experiment
as possible and use the `inherits_from` to point to this experiment as the parent. Then modify ONLY
what you have to create your new model/experiment. Using inheritance in this way reduces repetition, 
and ensure that if a setting is altered in the parent configuration, that it is propagated into yours.

Porting an imsi based model to a new machine
--------------------------------------------

Add a new machine, and compiler consideration for that machine in the `imsi-config/machines` directory.
You will need to figure out the details about required variables, models, scratch space to use, and 
you will need to download the model forcing files, and create the required DATAPATH_DB input database.
As for other additions, it is easiest to start from the most closely related machine and if possible,
use `inherits_from`.

Using imsi with a different model alltogether
---------------------------------------------

While imsi was originally create to configure models from the CCCma Integrated Modelling System,
it can be used to configure any model in principle. The key requirements are to create the 
`imsi-config` directory at the top level of the model code repository, and populate it with
the relavant `json` files that describe the configuration of the model. 

The imsi code itself has no knowledge about the underlying model. All model specific information
is injected through the json configuration.

