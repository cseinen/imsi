
# ChatGPT beauty
def replace_variables(obj, inputs):
    if isinstance(obj, dict):
        # If obj is a dictionary, iterate through its key-value pairs
        for key, value in obj.items():
            obj[key] = replace_variables(value, inputs)
        return obj
    elif isinstance(obj, list):
        # If obj is a list, iterate through its elements
        return [replace_variables(item, inputs) for item in obj]
    elif isinstance(obj, str):
        # If obj is a string, perform variable replacement
        return replace_variables_in_string(obj, inputs)
    else:
        # For other data types, return the object as is
        return obj

def replace_variables_in_string(s, inputs):
    # Define a function to replace variables in a string
    def replace(match):
        variable_name = match.group(1)
        if variable_name in inputs:
            return str(inputs[variable_name])  # Convert the value to a string
        else:
            return match.group(0)  # If variable not found, keep it as is

    import re
    # Use regular expression to find and replace variables in the string
    pattern = r'{{(.*?)}}'
    return re.sub(pattern, replace, s)

# Example usage:
json_data = {
    "name": "{{name}}",
    "info": {
        "age": "{{age}}",
        "city": "{{city}}"
    },
    "items": ["{{item1}}", "{{item2}}"]
}

inputs = {
    "name": "John",
    "age": 30,
    "city": "New York",
    "item1": "Apple",
    "item2": "Banana"
}

result = replace_variables(json_data, inputs)
print(result)





