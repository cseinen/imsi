"""
sequencers
===========

Provide a generic sequencer class, as an interface for dedicated sequencer sub-classes. This allows imsi to interface with multiple underlying
sequencers.
"""
import shutil
import os
from imsi import git_tools
import subprocess
from imsi.utils import get_date_string

#from imsi import iss # big enough to put in its own module.

def assign_sequencer(seq_name, sim):
    """
    Create and return a sequencer object
    """
    #supported_sequencers = {
    #    'iss' : imsi_sequencer_iss(sim),
    #    'maestro' : imsi_sequencer_maestro(sim)
    #}

    if seq_name == 'iss':
        from imsi.iss import imsi_sequencer_iss
        return imsi_sequencer_iss(sim)
    elif seq_name == 'maestro':    
        return imsi_sequencer_maestro(sim)
    else:
        raise ValueError(f"Specified sequencer {seq_name} not supported in imsi. Supported sequencers are: " +
                          ", ".join(supported_sequencers.keys()))

class imsi_sequencer():
    """
    A class that absracts properties for interacting  with different sequencers.

    In principle this class should provide the interface to imsi data, and any
    common required functions. Sub-classes for specific sequencers should then
    make use of the data, and provide all of the own specifically required methods.

    The sequencer specific implementation should expose back, through this interface,
    things such as the run command, etc.
    """
    def __init__(self, sim):
        self.sim = sim # Provides access to all imsi contained data
    
    def setup(self):
        """
        Any steps needed to setup the sequencer, including
        cloning source and creating any directories
        """
        raise NotImplementedError("sequencer config method must be defined")

    def config(self):
        """
        Steps needed to configure sequencer files based on updated imsi
        input. Examples might include editing resource files.
        """
        raise NotImplementedError("sequencer config method must be defined")

    def submit(self):
        """
        Steps needed to configure sequencer files based on updated imsi
        input. Examples might include editing resource files.
        """
        raise NotImplementedError("sequencer submit method must be defined")

class imsi_sequencer_maestro(imsi_sequencer):
    """
    The maestro sequencer cap
    """
    def __init__(self, simulation):
        super().__init__(simulation)
        self.sequencer = 'maestro'

    def setup(self, maestro_repo="git@gitlab.science.gc.ca:rcs001/canesm_maestro_suite.git", maestro_ver="suite-restructure"):
        """
        Any steps needed to setup the sequencer, including
        cloning source and creating any directories
        """
        # Here clone the required maestro repo. Source and location to be made flexible.
        git_tools.clone(maestro_repo, 'maestro-suite', ver=maestro_ver, recursive=False)

        # Make maestro dirs
        for dir in ['hub', 'logs', 'sequencing', 'stats', 'listings']:
            os.mkdir(dir)

        # copy or link required files from the maestro dir
        os.symlink(os.path.join('maestro-suite', 'modules'), './modules')
        os.symlink(os.path.join('maestro-suite', 'resources'), './resources')
        shutil.copyfile(os.path.join('maestro-suite', 'experiment.cfg'), 'experiment.cfg')
        
        # Some configuration
        subprocess.run(["makelinks", "-f"])

    def config(self):
        """
        Steps needed to configure sequencer files based on updated imsi
        input. Examples might include editing resource files.
        """
        pass
        # Should add here required configuration transfer from imsi to maestro
        # This would include things such as setting resources, timers, runid, etc.
        # Basically, anything configurable in maestro.
        #raise NotImplementedError("sequencer config method must be defined")

    def submit(self):
        """
        Steps needed to configure sequencer files based on updated imsi
        input. Examples might include editing resource files.
        """
        raise NotImplementedError("sequencer submit method must be defined")

