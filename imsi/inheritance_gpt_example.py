# From chatgpy, but does not really work as desired

def resolve_inheritance(data, dictionary):
    if isinstance(data, dict):
        if "inherits_from" in data:
            parent_dict_name = data["inherits_from"]
            if parent_dict_name in dictionary:
                parent_dict = dictionary[parent_dict_name]
                del data["inherits_from"]

                # Recursively resolve the parent first
                parent_dict = resolve_inheritance(parent_dict, dictionary)

                # Handle field addition and merging
                if "add" in data:
                    for field, value in data["add"].items():
                        if field in parent_dict:
                            # Merge the values into a list
                            if isinstance(parent_dict[field], list):
                                parent_dict[field].append(value)
                            else:
                                parent_dict[field] = [parent_dict[field], value]
                        else:
                            parent_dict[field] = value
                    del data["add"]

                # Handle field removal
                if "remove" in data:
                    for field in data["remove"]:
                        if field in parent_dict:
                            del parent_dict[field]
                    del data["remove"]

                # Handle field replacement, excluding fields from replacement
                if "replace" in data:
                    for field in data["replace"]:
                        if field in parent_dict:
                            data[field] = parent_dict[field]
                    del data["replace"]

                # Update with any remaining fields
                parent_dict.update(data)
                return parent_dict
            else:
                raise KeyError(f"Parent dictionary '{parent_dict_name}' not found in the provided dictionary.")
        else:
            # If no inheritance, process dictionary items recursively
            for key, value in data.items():
                data[key] = resolve_inheritance(value, dictionary)
            return data
    elif isinstance(data, list):
        # If it's a list, process list items recursively
        return [resolve_inheritance(item, dictionary) for item in data]
    else:
        # Base case: non-dictionary data, return as is
        return data

# Updated example JSON structure with inheritance and field instructions
data = {
    "dictA": {
        "inherits_from": "dictB",
        "replace": {"name": "john"},
        "add": {"city": "New York"},
        "remove": ["car"]
    },
    "dictB": {
        "name": "Alice",
        "age": 25,
        "city": "San Francisco",
        "car": "Toyota"
    }
}

resolved_data = resolve_inheritance(data.copy(), data)
print(resolved_data["dictA"])
