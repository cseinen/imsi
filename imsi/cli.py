"""
imsi CLI
--------

The entry-point console script that interfaces all users commands to imsi.

imsi has several categories of sub-commands. As this module develops further,
the sub-parsers will be implemented in the relevant downstream modules.

"""
import argparse
import sys
import os
import os.path
import subprocess
from imsi import imsi_main
from imsi import scheduler_tools

def main():
    """Console script for imsi."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')
    
    setup = subparsers.add_parser('setup', help='Setup rundirectory and clone source code')
    setup.add_argument('--runid', required=True, help='The name of the run to use. Typically a unique short string, not including "_" or special chars.')
    setup.add_argument('--repo', default='git@gitlab.com:cccma/canesm.git', help='url or path or repository to clone')
    setup.add_argument('--ver', default='develop_canesm', help='Version of the code to clone (commit, branch, tag...')
    setup.add_argument('--exp', default='cmip6-piControl', help='The experiment to run, contained under experiments directory in CanESM/CONFIG')
    setup.add_argument('--model', default=None, help='Optional. The model run, contained under model in CanESM/CONFIG. Can be changed later.')
    setup.add_argument('--fetch_method', default='clone', help='Defaults to "clone". Optionally specify "link" or "copy" to softlink or copy the code from disk instead of cloning')
    setup.add_argument('--seq', default='iss', help='Sequencer to use. Valid options are "iss" and "maestro". Defaults to "iss"')
                # Note the --seq arg could be modified to take additional args, e.g. for the maestro repo / ver

    config = subparsers.add_parser('config', help='Configure a simulation')
    update = subparsers.add_parser('update', help='Reload the repository based imsi configuration and update the simulation configuration')
    build = subparsers.add_parser('build', help='Build a simulation')
    submit = subparsers.add_parser('submit', help='Submit a simulation to run')

    list = subparsers.add_parser('list', help='List available configuration selections')
    list.add_argument('-r', '--repo', required=False, default=None, help='Model code directory containing imsi-config subdirectory with imsi json configurations')
    list.add_argument('-e', '--experiments', action='store_true', required=False, help='Supported experiment names')
    list.add_argument('-m', '--models', action='store_true', required=False, help='Supported model names')
    list.add_argument('-p', '--platforms', action='store_true', required=False, help='Supported machine names')
    list.add_argument('-c', '--compilers', action='store_true', required=False, help='Supported compiler names for this machine')
    list.add_argument('-o', '--options', nargs='?', default=False, const="no_opt", required=False, help='Available model options')
    
    summary = subparsers.add_parser('summary', help='Print summary of a current imsi simulation configuration')
    summary.add_argument('-d', '--detail', required=False, default=0, type=int, help='Integer level of detail for summary (1-3)')

    set = subparsers.add_parser('set', help='Set an imsi selection in the simulation configuration')
    set.add_argument('-f', '--file', required=False, help="The name of a json file containing the imsi selections.")
    set.add_argument('-s', '--selections', required=False, metavar="KEY=VALUE", nargs="+", help="A series of KEY=VALUE selection pairs to apply to the imsi simulation configuration")
    set.add_argument('-o', '--options', required=False, metavar="KEY=VALUE", nargs="+", help="A series of KEY=VALUE optin pairs to apply to the imsi simulation configuration")

    qstat = subparsers.add_parser('qstat', help='imsi formatted qstat')
    qstat.add_argument('-u', '--user', default=None, required=False, help="The name of a user to search queue by.")


    args = parser.parse_args()

    if args.command == 'setup':
        if (args.fetch_method != 'clone' and args.fetch_method != "link" and args.fetch_method !="copy"):
            print('\n\n**ERROR**: --fetch_methods must be one of "clone", "link" or "copy"\n')
            setup.print_help()
        else:
            imsi_main.setup_run(args.runid, args.repo, args.ver, args.exp, args.model, args.fetch_method, args.seq)
            
    
    if args.command == 'config':
        imsi_main.config_run()
        print("IMSI Config")

    if args.command == 'update':
        imsi_main.update_run()
        print("IMSI Update")

    if args.command == 'build':
        print("IMSI Build")
        imsi_main.build_run()        

    if args.command == 'submit':
        print("IMSI submit")
        imsi_main.submit_run()  

    if args.command == 'list':
        print("IMSI list")
        if not (args.experiments or args.models or args.platforms or args.compilers or args.options):
            print("\n**ERROR: You must select at least one category to list:** \n")
            list.print_help()
        else:
            imsi_main.list_choices(args.repo, args.experiments, args.models, args.platforms, args.compilers, args.options)  

    if args.command == 'summary':
        print("IMSI summary")
        imsi_main.summarize_run(args.detail)  
    
    if args.command == 'set':
        print("IMSI set")
        if  (args.file or args.selections or args.options):
            imsi_main.set_selections(args.file, args.selections, args.options)
        else:
            set.print_help()
            
    if args.command == 'qstat':
        print("IMSI qstat")
        scheduler_tools.pbs_q_query(user=args.user)

    if not args.command:
        parser.print_help()

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
