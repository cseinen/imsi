"""
nml_tools
=========

This is a module to collect common namelist functions.

This currently exists because neither f90nml nor the rpn nml tools correctly parse
the nemo namelists. Ideally all these functions would be replaced with more
robust, community packages.

NCS, 10/2021
"""
import re
from collections import OrderedDict

def nml_update(nml_input_file, nml_output_file, nml_changes): 
   """
   An interface function to update namelist parameters.

   Inputs:
      nml_input_file : str
         path to the namelist file to load in
      nml_output_file : str
         path to the updated namelist file to write
      nml_changes : dict
         A nested dict containing. At the top level the keys are the names of the namelists in nml_input_file, 
         the values are dicts containing key = value pairs to change in the namelist. 

   This is a basic python implementation of the `mod_nl` routine. Ideally the work would be done by a standard
   fortran namelist parser, such as f90nml. However, no existing parsers work correctly on NEMO namelists.
   """
   with open(nml_input_file, 'r') as infile:
      filedata = infile.read()

   # todo - it could be possible to preserve comments after the replacement by using a more
   # sophisticated re, or by doing a split on `!` like in the read function below.
   with open(nml_output_file, 'w') as outfile:
      for line in filedata.split('\n'):
         lineout = line
         for nam, namdict in nml_changes.items():
            for k, v in namdict.items():
               lineout = re.sub(rf' *{k} *=.*', f'{k} = {v}', lineout)
         outfile.write(f'{lineout}\n')
       

def nml_write(nml_output_file, nml): 
   """
   An interface function to write namelists from a dict.

   Inputs:
      nml_output_file : str
         path to the namelist file to write
      nml : dict
         A nested dict containing. At the top level the keys are the names of the namelists, 
         the values are dicts containing key = value pairs of the namelist. 

   Ideally the work would be done by a standard fortran namelist parser, such as f90nml. 
   However, no existing parsers work correctly on NEMO namelists.
   """
   with open(nml_output_file, 'w') as outfile:
      for nam, namdict in nml.items():
         outfile.write(f'&{nam}\n')
         for k, v in namdict.items():
            outfile.write(f'{k} = {v}\n')
         outfile.write(f'/\n')

def nml_read(nml_input_file): 
   """
   An interface function to read namelists into a dict.

   Inputs:
      nml_input_file : str
         path to the namelist file to read

   Ideally the work would be done by a standard fortran namelist parser, such as f90nml. 
   However, no existing parsers work correctly on NEMO namelists.
   """
   with open(nml_input_file, 'r') as infile:
      filedata = infile.read()

   nml = OrderedDict()
   for line in filedata.split('\n'):
      if line.strip().startswith('&'):
          key = re.sub(r'!.*', '', line).replace('&','').strip()
          nml[key] = {}
          continue
          print(key)
      if line.strip().startswith('/'):
         key = 'ERROR'
         continue      
      if line.strip().startswith('!'):
         continue
      if line:
         key_value_comment = line.split('!',1)
         key_value = key_value_comment[0]
         if len(key_value_comment)>1:
            comment = key_value_comment[1]
         else:
            comment = ''
         k = re.sub(r'=.*', '', key_value).strip()
         v = re.sub(r'.*=', '', key_value).strip()
         nml[key][k] = v.strip()
         #print(f'{k} = {v} !{comment}')
   return nml