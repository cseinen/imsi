"""
git_tools
=========

This is a module to collect common version control operations.

It might be worth checking out e.g. https://gitpython.readthedocs.io/en/stable/

However, for now to reduce dependencies, and given the basic operations needed, 
I'm just implementing a few functions here.

NCS, 10/2021
"""
import os
import subprocess
from collections import OrderedDict

def recursive_clone(repo_address, local_name, ver):
    """
    Clone a git repository recursively, and consistently
    checkout a specified version across all submodules.

    Inputs:
        repo_address : str
            The address to clone from, typically a url.
        local_name : str
            The name to use for the on disk code
        ver : str
            The reference to checkout
    """
    cwd = os.getcwd()
    gitout = subprocess.run(["git", "clone", "--recursive", repo_address, local_name])
    os.chdir(local_name)
    gitout = subprocess.run(["git", "checkout", "--recurse-submodules", ver])
    gitout = subprocess.run(["git", "submodule", "update", "--recursive", "--init", "--checkout"]) 
    os.chdir(cwd)
    
def clone(repo_address, local_name, ver=None, recursive=True):
    """

    ** general implementation, not functional**

    Clone a git repository, optionally recursively, and consistently
    checkout a version, across all submodules if needed.

    Inputs:
        repo_address : str
            The address to clone from, typically a url.
        local_name : str
            The name to use for the on disk code
        ver : str
            The reference to checkout
        recursive : boolean
            Whether to recursively clone or not
    """

    # Trying to be smart and collect all commands in an ordered dict, to
    # avoid repeated if branching. However, it is not clear how to mix 
    # python and os commands.
    if recursive:
        recursive_clone(repo_address, local_name, ver)
    else:   
        cwd = os.getcwd()
        gitout = subprocess.run(["git", "clone", repo_address, local_name])
        os.chdir(local_name)
        if ver:
            gitout = subprocess.run(["git", "checkout", ver])
        os.chdir(cwd)

if __name__ == '__main__':
    SystemExit('Version control module not directly callable')