import abc

class scheduler(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        pass


class scheduler():
    def __init__(self, scheduler_name):
        self.type = scheduler_name
        print("Init scheduler")
        choose_scheduler_type(self.type)

    @staticmethod
    def choose_scheduler_type(scheduler_name):
        if scheduler_name == 'pbs':
            return pbs_scheduler()
        elif scheduler_name == 'slurm':
            return slurm_scheduler()

class pbs_scheduler():
#    def set_walltime():
#        pass
#    def set...
    pass

class slurm_scheduler():
    pass
