"""
Imsi shell sequencer (ISS)
===========================

The ISS is a simple, highly portable shell batch sequencer for running model simulations,
and built directly adjacent to the main imsi source. 

The ISS provides a simple time-looping mechanism to handle job resubmission, and supports
submission of downstream dependencies. The iss leverages knowledge from :mod:`imsi.scheduler_tools`
to support submissions to different batch schedulers. 
"""
import shutil
import os
import stat
from imsi.sequencers import imsi_sequencer
import subprocess

class imsi_sequencer_iss(imsi_sequencer):
    """
    The imsi shell sequencer cap
    """
    def __init__(self, simulation):
        super().__init__(simulation)
        self.sequencer = 'iss'    

    def setup(self):
        pass

    def config(self):
        """
        Write files needed for the imsi shell sequencer, including
        the `.simulation.time.state` file, and the submission files
        for the model and diagnostics.
        """
        # Update to include explicit paths to write files at.
        os.chdir(self.sim.run_config['work_dir'])
        self.__write_submission_file()
        self.__write_diag_submission_file()

        listings_dir = os.path.join(self.sim.run_config['work_dir'], 'listings')
        if not os.path.exists(listings_dir):
            os.mkdir(listings_dir)
        
        os.chdir(os.path.join(self.sim.run_config['work_dir'], 'config'))
        self.__write_time_parameters()

        # Write file with model run commands from machine config
        # This is called within a more generic model run script
        # copied directly below.
        self.__write_run_cmd_file()

        # Possibly this should be inside a config for the whole coupled model?
        # In other words, files to extract should be listed in a json, not hardcoded.
        shutil.copyfile(os.path.join(self.sim.imsi_config_path, 'models', self.sim.model_config['model_run_script']),
                        self.sim.model_config['model_run_script'])
        
        shutil.copyfile(os.path.join(self.sim.imsi_config_path, 'diagnostics', self.sim.model_config['diagnostic_run_script']),
                        self.sim.model_config['diagnostic_run_script'])

    def submit(self):
        self.__run()

    def __write_time_parameters(self):
        """
        This creates a .simulation.time.state file for the simulation, that contains
        time variable definitions required by downstream shell scripting.
        """
        # Contrived to make compile_canesm.sh work. Should be deprecated.
        with open(f'.simulation.time.state', 'w') as f: #_{self.machine}_{self.compiler}
            f.write("# Imsi created shell timing file\n")
            f.write(f"START_YEAR={self.sim.compset_config['start_year']}\n")
            f.write(f"END_YEAR={self.sim.compset_config['end_year']}\n")
            f.write(f"CURRENT_YEAR={self.sim.compset_config['start_year']}\n")
            f.write(f"DIAG_YEAR={self.sim.compset_config['start_year']}\n")
            f.write(f"MODEL_RESUBMIT=FALSE\n")
            f.write(f"DIAG_RESUBMIT=FALSE\n")


    def __write_path_configration(self):
        def write_mk_and_link(path, f):
            f.write(f"mkdir -p {path}\n")
            f.write("mkdir -p ${WRK_DIR}/hub ; cd ${WRK_DIR}/hub\n")
            f.write(f"ln -s {path} .\n")

        with open(f'path_configuration.sh', 'w') as f: 
            f.write("# Imsi created path config file\n")
            datestr = get_date_string()
            f.write(f"# Created for machine: {self.sim.machine} on date: {datestr}\n")
            write_mk_and_link("$EXEC_STORAGE_DIR", f)
            write_mk_and_link("$RUNPATH", f)


    def __write_run_cmd_file(self):
        """Only for testing. Should potentially be put directly into the run file?
        """
        with open(f'imsi-model-run.sh', 'w') as f:
            f.write("#!/bin/bash\n")
            for cmd in self.sim.machine_config["run_commands"]:
                f.write(f"{cmd}\n")
        st = os.stat('imsi-model-run.sh')
        os.chmod('imsi-model-run.sh', st.st_mode | stat.S_IEXEC)

    def __write_submission_file(self):
        """
        Writes a basic shell submission file (current pbs or slurm),
        with resubmission logic, relying on config/.simulation.time.state
        time tracking.  
        This is effectively a bootstraped imsi/shell sequencer. 
        """
        user_script_to_source = f"{self.sim.run_config['work_dir']}/config/{self.sim.model_config['model_run_script']}"
        directives = self.sim.machine_config["batch_commands"]["directives"]
        diag_dependency = f"{self.sim.run_config['work_dir']}/imsi-diagnostics-{self.sim.run_config['runid']}.sh"
        script_full_path = os.path.join(self.sim.run_config['work_dir'], f'imsi-submission-{self.sim.run_config["runid"]}.sh')
        self.sim.scheduler.write_simulation_sequencing_file(script_name=script_full_path,
                                                        job_identifier='imsi_run_canesm', user_script_to_source=user_script_to_source,
                                                        directives=directives, dependencies=[diag_dependency], cycle_flag="MODEL_RESUBMIT")

    def __write_diag_submission_file(self):
        """
        Writes a basic shell submission file for diagnostics.
        """
        user_script_to_source = f"{self.sim.run_config['work_dir']}/config/{self.sim.model_config['diagnostic_run_script']}"    
        script_full_path = os.path.join(self.sim.run_config['work_dir'], f'imsi-diagnostics-{self.sim.run_config["runid"]}.sh')
    
        self.sim.scheduler.write_simulation_sequencing_file(script_name=script_full_path,
                                                        job_identifier='imsi_diag_canesm', user_script_to_source=user_script_to_source,
                                                        cycle_flag='DIAG_RESUBMIT')

    def __run(self):
        """
        Submits the runs batch job to the queue. Could be configured to interface with
        different sequencers in the future as needed.
        """
        # Although this seems a little redundant (chdir and fullpath), partly this is done
        # to control where the output listings go. Ideally, we should specify a place for
        # the listing explicitly in the submission.
        wrk_dir=self.sim.run_config['work_dir']
        os.chdir(wrk_dir)
        cmd = self.sim.scheduler.submission_command
        submission_fullpath = os.path.join(wrk_dir, f"imsi-submission-{self.sim.run_config['runid']}.sh")
        subprocess.run(cmd.split() + [submission_fullpath])